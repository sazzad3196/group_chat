package Socket;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Server {
	 ServerSocket serverSocket = null;
	 List<Socket> list = new ArrayList<Socket>();
	 
	 Server(int port) {
		 try {
			 serverSocket = new ServerSocket(port);
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		 System.out.println("Server started"); 
		 System.out.println("Waiting for a client ..."); 
		 while(true) {
			 
			 Socket socket = null;
			 try {
				 socket = serverSocket.accept();
				 System.out.println("Client accepted: " + socket); 
				 list.add(socket);
				 Thread thread = new ServerSideHandler(socket, this, list);
				 thread.start();
			 }catch (Exception e) {
				// TODO: handle exception
				 try {
				   socket.close();
				 }catch (Exception i) {
					// TODO: handle exception
					 i.printStackTrace();
				}
				 e.printStackTrace();
			 }
		 }
		 
	 }
	 
	 
	 public static void main(String[] args) {
		  Server server = new Server(700);
	 }
	 
}
