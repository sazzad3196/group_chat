package Socket;

import java.awt.TextArea;
import java.io.DataInputStream;
import java.net.Socket;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ClientSideHandler extends Thread{
	Socket socket = null;
	DataInputStream ob = null;
	TextArea tArea = null;
	
	public ClientSideHandler(Socket socket, TextArea tArea) {
		this.socket = socket;
		this.tArea = tArea;
	}
	
	public void run() {
		    try {
				 DataInputStream ob = new DataInputStream(socket.getInputStream());
				 while(true) {
					 if(ob.available() == 0) {
						 Thread.sleep(100);
						 continue;
					 }
			
					 byte[] lengthArray = new byte[2];
					 lengthArray[0] = ob.readByte();
					 lengthArray[1] = ob.readByte();
					 int high = lengthArray[1] >= 0 ? lengthArray[1] : 256 + lengthArray[1];
					 int low = lengthArray[0] >= 0 ? lengthArray[0] : 256 + lengthArray[0];
					 int length = low | (high << 8);
					 byte[] buffer = new byte[512];
					 int bytesRead = 0;
					 String str = "";
					 int totalRead = 0;
					 
					 while((bytesRead = ob.read(buffer)) != -1 ) {
						 str += new String(buffer, 0, bytesRead);
						 totalRead += bytesRead;
						 if(totalRead >= length) {
							 break;
						 }
					 }
					 //System.out.println("Server send message: " + str);
					 JsonObject jsonObject = new JsonParser().parse(str).getAsJsonObject();
					 String name = jsonObject.get("name").getAsString();
					 String message = jsonObject.get("message").getAsString();
					 tArea.setText(tArea.getText() + "\n\r" + name + ": " + message);
					 //System.out.println(name + " send: " + message);
				 }
			 
		   }catch (Exception e) {
			   e.printStackTrace();
		   }	 
	}
}
