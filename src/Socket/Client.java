package Socket;

import java.awt.Button;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.color.CMMException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.google.gson.Gson;

public class Client{
	 private String name = null;
	 private Socket socket = null;
	 private DataInputStream dis = null;
	 private BufferedOutputStream bos = null;
	 private DataOutputStream dos = null;
	 
	 /*public Client(String address, int port) {
		 dis = new DataInputStream(System.in);
		 Frame frame = new Frame("Client Side");
		 
		 try {
			 System.out.print("Enter client name: ");
			 name = dis.readLine();
			 
			 socket = new Socket(address, port);
			 System.out.println("Connected");
			 Thread thread = new ClientSideHandler(socket);
			 thread.start();

			 dos = new DataOutputStream(socket.getOutputStream());
		 } catch(UnknownHostException u) { 
	         System.out.println(u); 
	     } 
	     catch(IOException i) {
	         System.out.println(i); 
	     } 
		 
		 try {
			 while(true) {
				 System.out.print("Please enter message: ");
				 String message = dis.readLine();
				 MessageClass mClass = new MessageClass(name, message);
				 Gson gson = new Gson();
				 String line = gson.toJson(mClass);
				 //System.out.println(line);
				 byte[] msg = line.getBytes();
				 byte[] array = new byte[msg.length + 2];
				 array[0] = (byte) (msg.length & 0xFF);
				 array[1] = (byte) ( (msg.length >> 8 ) & 0xFF);
				 System.arraycopy(msg, 0, array, 2, msg.length);
				 dos.write(array);
			 }
		 } catch (Exception e) {
			e.printStackTrace();
		}
		 //Close Connection
		 try {  
             dos.close(); 
             dis.close();
	     } 
	     catch(IOException i)  { 
	         System.out.println(i); 
	     } 
	 }*/
	 
	 
	 public Client(String address, int port) {
		 Frame frame = new Frame("Client Side");
		 TextField tf1 = new TextField();
		 tf1.setBounds(50,100, 300,30);
		 
		 Button button1 = new Button("Connected");
		 button1.setBounds(50,145,200,40);
		 
		 button1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				 name = tf1.getText();
				 if(!name.equals("")) {
					 TextField tf2 = new TextField();
					 tf2.setBounds(50,240, 300,30);
					 TextArea tArea = new TextArea();
					 tArea.setBounds(50, 400, 300, 200);
					 tArea.setEditable(false);
					 Button button2 = new Button("Send");
					 button2.setBounds(50,300,200,40);
					 button2.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							String message = tf2.getText();
							if(!message.equals("")) {
								try {
									 //while(true) {
										 MessageClass mClass = new MessageClass(name, message);
										 Gson gson = new Gson();
										 String line = gson.toJson(mClass);
										 //System.out.println(line);
										 byte[] msg = line.getBytes();
										 byte[] array = new byte[msg.length + 2];
										 array[0] = (byte) (msg.length & 0xFF);
										 array[1] = (byte) ( (msg.length >> 8 ) & 0xFF);
										 System.arraycopy(msg, 0, array, 2, msg.length);
										 dos.write(array);
										 tArea.setText(tArea.getText() + "\n\r" + name + ": " + message);
									 //}
								 } catch (Exception p) {
									p.printStackTrace();
								}
							}
							tf2.setText("");
							
						}
					});
					 
					 
					 try {
						 socket = new Socket(address, port);
						 Thread thread = new ClientSideHandler(socket, tArea);
						 thread.start();
						 tf1.setEnabled(false);
						 button1.setEnabled(false);
						 dos = new DataOutputStream(socket.getOutputStream());
					 } catch(UnknownHostException u) { 
				         System.out.println(u); 
				     } 
				     catch(IOException i) {
				         System.out.println(i); 
				     } 
					 
					 frame.add(tArea);
					 frame.add(tf2);
					 frame.add(button2);
				 }
				
			}
		});
		 
		frame.add(tf1);
		frame.add(button1);
		frame.setSize(400, 650);
		frame.setLayout(null);
		frame.setVisible(true);
		 
	 }
	
	 
	 public static void main(String[] args) {
		  Client client = new Client("localhost", 700);
	 }
}  
