package Socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ServerSideHandler extends Thread{
	Socket socket = null;
	 Server server = null;
	 List<Socket> list = new ArrayList<>();

	 public ServerSideHandler(Socket socket, Server server, List<Socket> list) {
		 this.socket = socket;
		 this.server = server;
		 this.list = list; 
	 }

	 public void run() {
		 DataInputStream bis = null;
		 try {
			 bis = new DataInputStream(socket.getInputStream());
			 while(true) {
				 if(bis.available() == 0) {
					 Thread.sleep(100);
					 continue;
				 }
				 byte[] lengthArray = new byte[2];
				 lengthArray[0] = bis.readByte();
				 lengthArray[1] = bis.readByte();
				 int high = lengthArray[1] >= 0 ? lengthArray[1] : 256 + lengthArray[1];
				 int low = lengthArray[0] >= 0 ? lengthArray[0] : 256 + lengthArray[0];
				 int length = low | (high << 8);
				 //System.out.println("Len: " + length);
	
				 byte[] buffer = new byte[512];
				 int bytesRead = 0;
				 String line = "";
				 int totalRead = 0;
				 
				 while((bytesRead = bis.read(buffer)) != -1 ) {
					 line += new String(buffer, 0, bytesRead);
					 totalRead += bytesRead;
					 if(totalRead >= length) {
						 break;
					 }
				 }
				 // String convert to Json object
				 //System.out.println("This Client Message: " + line);
				 JsonObject jsonObject = new JsonParser().parse(line).getAsJsonObject();
				 String name = jsonObject.get("name").getAsString();
				 String message = jsonObject.get("message").getAsString();
				 System.out.println(name + " send: " + message);
				 
				 for(int i=0; i<list.size(); i++) {
					 try {
					   Socket newSocket = list.get(i);	
					   if(socket != newSocket) {
						   DataInputStream iStream = new DataInputStream(newSocket.getInputStream());
						   if(iStream.available() != 0) {
							   int len = 0, totalLen = 0;
							   byte[] buf = new byte[512];
							   while( (len = iStream.read(buf)) != -1) {
								   totalLen += len;
							   }
							   System.out.println("Total: " + totalLen);
						   }
						   else {
							   System.out.println("Data: " + iStream.available());
							   DataOutputStream object = new DataOutputStream(newSocket.getOutputStream());
							   byte[] msg = line.getBytes();
							   byte[] array = new byte[msg.length + 2];
							   array[0] = (byte) (msg.length & 0xFF);
							   array[1] = (byte) ( (msg.length >> 8 ) & 0xFF);
							   System.arraycopy(msg, 0, array, 2, msg.length);
							   object.write(array);
						   }
					   }
					 } catch (Exception e) {
						 e.printStackTrace();
					}
				 }
			 }
		 }catch (Exception e) {
			// TODO: handle exception
			 e.printStackTrace();
		}
		try {
			bis.close();
			socket.close();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	 }
}
